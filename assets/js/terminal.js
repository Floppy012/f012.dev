class Terminal {

  constructor(elementId) {
    this.element = this._injectDOM(elementId);
    this.content = this.element.find(".content");
    this.drag = false;
    this.lastCursorPos = null;
    this.cmdLine = this.element.find("#current #text");
    this.inputArea = this.element.find("#inputArea");
    this.commands = null;
    this.files = null;
    this.shuttingDown = false;
    this.commandHistory = [];
    this.cmdHistoryIndex = undefined;
    this._registerEvents();
    this._loadCommands();
    this._loadFiles();
  }

  _injectDOM(elementId) {
    var element = `
    <div id="term1" class="terminal">
      <div class="titlebar">
        <div class="balls">
          <div class="ball red" id="closeButton"></div>
          <div class="ball yellow"></div>
          <div class="ball green"></div>
        </div>
        <div class="title">webuser@f012.dev: /</div>
      </div>
      <textarea rows="0" cols="0" id="inputArea"></textarea>
      <div class="content">
        <ul>
            <li id="current"><span class="sol">/</span><span class="blinking-cursor" id="text"></span></li>
        </ul>
      </div>
    </div>
    `;

    var term = $(element);
    term.attr('id', elementId);
    term.hide();
    $(document.body).append(term);
    return term;
  }

  _loadCommands() {
    var self = this;
    this.commands = {
      help: function() {
        var keys = Object.keys(self.commands).join(", ");
        return `
        You can try to use one of these commands:<br>
        ${keys}
        `;
      },
      cat: function(args) {
        if (args.length === 0) {
            return "Try 'cat --help' for more information.";
        }

        switch (args[0]) {
          case "--help":
            return `
              <b>*insert smallest unit you know*</b>-cat written by me (Floppy012). What a surprise!<br>
              <br>
              Usage: cat [FILE]<br>
              <br>
              Unfortunately I didn't have time to add more features to this poor copy of the real cat :)
            `;
          default:
            if (!(args[0] in self.files)) {
              return `cat: ${args[0]}: No such file or directory`;
            }

            return self.files[args[0]].getContents();
        }
      },
      rm: function() {
        return "plz dont";
      },
      reboot: function() {
        return "reboot is for noobs. Shut it down and power it back on!";
      },
      dir: function() {
        return "I can sense the smell of a Windows user ( ͡° ͜ʖ ͡°) (use ls)";
      },
      ls: function() {
        var size = Object.keys(self.files).length;
        var out = `total ${size}`;
        for (var key in self.files) {
          var file = self.files[key];
          out += `<br>${file.permissions} ${file.hardlinks} ${file.user} ${file.group} ${file.size} ${key}`;
        }
        return out;
      },
      clear: function() {
        self.clear();
        return false;
      },
      shutdown: function() {
        if (self.shuttingDown) return "U nervous? Stop clicking. The algorithm wears off (╯°□°）╯︵ ┻━┻"
        self.shuttingDown = true;
        setTimeout(() => {
          self.close();
          self.clear();
          self.shuttingDown = false;
        }, 1500);

        return "Bye!";
      },
      cd: function() {
        return "Yo! Don't overestimate me.";
      },
      exit: function() {
        self.sendCommand("shutdown", false);
        return "... I know. Shitty solution. But it gets the job done!"
      }
    };
  }

  _loadFiles() {
    this.files = {
      'id_rsa.pub': {
        permissions: "-rw-rw-rw",
        hardlinks: 1,
        user: 'florian',
        group: 'florian',
        size: 28,
        getContents:  function() {
          var tmp = null;
          jQuery.ajax({
            url: 'dl/secure_rsa.pub',
            success: function(result) {
              tmp = result;
            },
            async: false
          });
          return tmp;
        }
      }
    }
  }

  _mousedown(event) {
    this.drag = true;
    this.lastCursorPos = [event.pageX, event.pageY];
  }

  _mousemove(event) {
    if (!this.drag) return;
    var cursorPos = [event.pageX, event.pageY];
    var topleft = this.element.position();
    var deltaX = cursorPos[0] - this.lastCursorPos[0];
    var deltaY = cursorPos[1] - this.lastCursorPos[1];

    this.lastCursorPos = cursorPos;

    this.element.css('top', topleft.top + deltaY + "px");
    this.element.css('left', topleft.left + deltaX + "px");
  }

  _mouseup(event) {
    this.drag = false;
  }

  _input(event) {
    this.cmdLine.text(this.inputArea.val());
  }

  _keydown(event) {
    var code = event.keyCode;
    if (code == '9') {
      this.writeOut("I'm sorry Dave, I'm afraid I can't do that.");
      event.preventDefault();
    }
    if (code == '35' || code == '36' || code == '37' || code == '38' || code == '39' || code == '40')
      event.preventDefault();

    if (code == '38') {
      const index = this._getCommandHistoryIndex();

      if (index < 0) {
        return;
      }

      this.cmdLine.text(this.commandHistory[index]);
      this.cmdHistoryIndex = index -1;
    } else if (code == '40') {
      const index = this._getCommandHistoryIndex();

      if (index + 1 >= this.commandHistory.length) {
        this.cmdLine.text("");
        return;
      }

      this.cmdLine.text(this.commandHistory[index + 1]);
      this.cmdHistoryIndex = index + 1;
    }
  }

  _keypress(event) {
    if (event.which != 13)
      return;
    event.preventDefault();

    this._execCommand();
  }

  writeOut(value, html) {
    var cmdReturn = $("<li class=\"response\"></li>");
    if (html) {
      cmdReturn.html(value);
    } else {
      cmdReturn.text(value);
    }

    cmdReturn.insertBefore(this.cmdLine.parent());
  }

  clear() {
    this.element.find("li").not("#current").remove();
  }

  close(callback) {
    this.content.hide();
    this.element.fadeOut(400, callback);
  }

  open(callback) {
    this.content.show();
    this.element.fadeIn(400, callback);
  }

  async sendCommand(command, typeIn) {
    if (typeIn) {
      this.cmdLine.text("");
      await this.cmdLine.addText(command, true);
      this.inputArea.val(this.cmdLine.text());
    } else {
      this.inputArea.val(command);
      this.cmdLine.text(this.inputArea.val());
    }

    this._execCommand();
  }

  _getCommandHistoryIndex() {
    return this.cmdHistoryIndex !== undefined ? this.cmdHistoryIndex : this.commandHistory.length - 1;
  }

  _execCommand() {
    var cmdParent = this.cmdLine.parent();
    var clone = cmdParent.clone();
    clone.removeAttr("id");
    clone.find("#text").removeClass("blinking-cursor");
    clone.insertBefore(cmdParent);

    var cmdVal = this.inputArea.val();
    this.inputArea.val("");
    this.cmdLine.text(this.inputArea.val());
    this.commandHistory.push(cmdVal);
    this.cmdHistoryIndex = undefined;
    if (cmdVal.length > 0) {
      var cmdSplit = cmdVal.split(" ");
      var cmdName = cmdSplit[0];
      cmdSplit.shift();

      if (cmdName in this.commands) {
        var cmdReturnVal;
        try {
          cmdReturnVal = this.commands[cmdName](cmdSplit)
        } catch (ex) {
          cmdReturnVal = '<font color="#FF0000"><b>Internal error ...</b></font> plz report bug <a href="https://gitlab.com/Floppy012/f012.dev/issues">*click*</a>';
          console.error(ex);
        }
        if (cmdReturnVal)
          this.writeOut(cmdReturnVal, true);
      } else {
        this.writeOut(cmdName + ": command not found");
      }
    }

    this.content.scrollTop(this.content[0].scrollHeight);
  }

  _registerEvents() {
    this.element.find(".titlebar").mousedown((e) => this._mousedown(e));
    $(window).mouseup((e) => this._mouseup(e));
    $(window).mousemove((e) => this._mousemove(e));
    this.inputArea.on('input', (e) => this._input(e));
    this.inputArea.keypress((e) => this._keypress(e));
    this.inputArea.keydown((e) => this._keydown(e));
    this.element.click((e) => {
      if (window.getSelection().type != "Range")
        this.inputArea.focus()
    });
    this.element.find("#closeButton").click(() => this.sendCommand("shutdown"));
  }

}
