jQuery.fn.clearLine = function(callback) {
  var element = $(this[0]);
  return new Promise(resolve => {
    if (element.hasClass("blinking-cursor")) {
      resolve(false);
      return;
    }
    startEdit(element);
    emptyLine(element, () => {
      stopEdit(element);
      resolve(true);
    });
  });
}

jQuery.fn.addText = function(string, force) {
  var element = $(this[0]);
  return new Promise(resolve => {
    if (element.hasClass("blinking-cursor") && !force) {
      resolve(false);
      return;
    }
    startEdit(element);
    addChars(element, string.split(''), () => {
      stopEdit(element);
      resolve(true);
    });
  });
}

function startEdit(element) {
  element.addClass("blinking-cursor");
}

function stopEdit(element) {
  element.removeClass("blinking-cursor");
}

function emptyLine(element, callback) {
  if (element.text().length === 0) {
    if (callback) callback();
    return;
  }

  removeChar(element);
  var rand = random(50, 150);
  setTimeout(() => emptyLine(element, callback), rand);
}

function addChars(element, chars, callback) {
  if (chars.length === 0) {
    if (callback) callback();
    return;
  }

  addChar(element, chars[0]);
  chars.shift();
  setTimeout(() => addChars(element, chars, callback), random(50, 150));
}

function removeChar(element) {
  if (element.text().length === 0)
    return;
  var text = element.text();
  element.text(text.substring(0, text.length - 1));
}

function addChar(element, char) {
  element.text(element.text() + char);
}

function random(min, max) {
  return Math.floor(Math.random() * max + min);
}
